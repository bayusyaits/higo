import { createStore } from 'vuex';
/* eslint-disable */
import opentDBService from '@/helpers/services/opentdb-service'
import lumenService from '@/helpers/services/lumen-service'

const state = () => ({
    app: {
        doc: require('../../package.json').version,
        copyright: process.env.VUE_APP_COPYRIGHT
    },
    questions: [],
    answer: {
        email: null,
        role: null,
        score: 0,
        data: []
    },
    score: 0
})

const actions = {
    getOpentDB(_state, request) {
        return opentDBService.get(request)
    },
    getData(_state, request) {
        return lumenService.get(request)
    },
    postData(_state, request) {
        return lumenService.post(request)
    }
}

const mutations = {
    initQuestions(_state, value) {
        _state.questions = value
    },
    initAnswer(_state, value) {
        _state.answer = value
    },
    initAnswerSpecifict(_state, data) {
        const keys = Object.keys(data)
        const value = Object.values(data)
        _state.answer[keys[0]] = value[0]
    },
    initScore(_state, value) {
        _state.answer = value
    }
}

export default createStore({
    state,
    mutations,
    actions
});