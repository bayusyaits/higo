/* eslint-disable */
const axios = require('axios')
const moment = require('moment')

class Http {
    constructor() {
        this.appKey = process.env.VUE_APP_KEY
    }

    generateHeaders(contentType, timeStamp, lang) {
            let headers = {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': contentType,
                'Accept': contentType,
                'Accept-Language': lang
            }
            if (localStorage.getItem('loginData')) {
                const loginData = JSON.parse(localStorage.getItem('loginData'))
                headers = {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': contentType,
                    'Accept': contentType,
                    'Accept-Language': lang
                }
            }
            return headers
        }
        /**
         * how to use
         * http.get()
         */
    async get(url, params, lang) {
        let uri = url
        const esc = encodeURIComponent
        if (params) {
            const query = Object.keys(params)
                .map((k) => `${esc(k)}=${esc(params[k])}`)
                .join('&')

            uri = `${url}?${query}`
        }

        // generate headers for this request
        const timeStamp = moment().format('YYYY-MM-DDTHH:mm:ss')
        const headers = this.generateHeaders('application/json', timeStamp, lang)
        const response = await new Promise((resolve, reject) => {
            resolve(
                axios({
                    method: 'get',
                    url: uri,
                    timeout: 60 * 2 * 1000,
                    headers
                })
                .catch(() => reject)
            )
        })

        return response
    }

    async delete(url, params, lang) {
        let uri = url
        const esc = encodeURIComponent
        if (params) {
            const query = Object.keys(params)
                .map((k) => `${esc(k)}=${esc(params[k])}`)
                .join('&')
            uri = `${url}?${query}`
        }

        // generate headers for this request
        const timeStamp = moment().format('YYYY-MM-DDTHH:mm:ss')
        const headers = this.generateHeaders('application/json', timeStamp, lang)
        const response = await new Promise((resolve, reject) => {
            resolve(
                axios({
                    method: 'delete',
                    url: uri,
                    timeout: 60 * 2 * 1000,
                    headers
                })
                .catch(() => reject)
            )
        })

        return response
    }
    async post(url, data, lang = 'id') {
        let payload
        let contentType

        if (!data.file) {
            payload = JSON.stringify(data)
            contentType = 'application/json'
        } else {
            payload = new FormData()
            payload.append('file', data.file)
            payload.append('data', JSON.stringify(data.parameter))
            contentType = 'multipart/form-data'
        }

        const timeStamp = moment().format('YYYY-MM-DDTHH:mm:ss')
        let headers = this.generateHeaders(contentType, timeStamp, lang)

        const response = await new Promise((resolve, reject) => {
            resolve(
                axios({
                    method: 'post',
                    url,
                    data: payload,
                    proxy: {
                        host: '1',
                        port: 3128
                    },
                    timeout: 60 * 2 * 1000,
                    headers
                })
                .catch(() => reject)
            )
        })

        return response
    }

    async put(url, data, lang) {
        const payload = JSON.stringify(data)
            // generate signature & headers for this request
        const timeStamp = moment()
            .format('YYYY-MM-DDTHH:mm:ss')

        const headers = this.generateHeaders(
            'application/json',
            timeStamp,
            lang
        )

        const response = await new Promise((resolve, reject) => {
            resolve(
                axios({
                    method: 'put',
                    url,
                    data: payload,
                    proxy: {
                        host: '1',
                        port: 3128
                    },
                    timeout: 60 * 2 * 1000,
                    headers
                })
                .catch(() => reject)
            )
        })

        return response
    }
}
export default new Http()