/* eslint-disable */
import http from '@/helpers/http-helper'

class LumenService {
    constructor() {
        this.url = process.env.VUE_APP_API_URL || 'http://localhost:8090'
        this.version = process.env.VUE_APP_API_VERSION || '1.0.1'
        this.version = this.version.substring(0, 1)
        this.baseUrl = `${this.url}/v${this.version}`
    }

    /**
     * Content
     * @param {*} params
     */
    get(params, lang = 'id') {
        let url = `${this.baseUrl}`
        const scope = ['marketplace']
        if (params && params.uri) {
            url = `${url}/${params.uri}`
        }
        return http.get(url, params, lang)
    }

    post(payload, lang = 'id') {
        let url = `${this.baseUrl}`
        if (payload && payload.uri) {
            url = `${url}/${payload.uri}`
            delete payload.uri
        }
        return http.post(url, payload, lang)
    }

    put(payload, lang = 'id') {
        let url = `${this.baseUrl}`
        if (payload && payload.scope) {
            url = `${url}/${payload.scope}`
        }
        return http.put(url, payload, lang)
    }
}
export default new LumenService()