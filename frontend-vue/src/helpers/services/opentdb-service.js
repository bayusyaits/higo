/* eslint-disable */
const axios = require('axios')

class OpentDBService {
    constructor() {
        this.url = `https://opentdb.com`
    }

    /**
     * Content
     * @param {*} params
     */
    async get(data) {
        let url = this.url
        if (data && data.uri) {
            url = `${url}/${data.uri}`
        }
        const contentType = 'text/html'
        const response = await new Promise((resolve, reject) => {
            resolve(
                axios({
                    method: 'get',
                    url,
                    headers: {
                        'Content-Type': contentType
                    }
                })
                .catch(() => reject)
            )
        })

        return response
    }
}
export default new OpentDBService()