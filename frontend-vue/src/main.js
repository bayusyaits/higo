import { createApp } from 'vue';
import { Field, Input } from 'buefy';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import 'buefy/dist/buefy.css';
/* eslint-disable */
createApp(App).use(store).use(Field).use(Input).use(router).mount('#app');