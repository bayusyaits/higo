<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->increments('contentId');
            $table->json('contentValue')->comment('{"id": {"description": "description","title": "penjelasan"}, "en": {"description": "description","title": "penjelasan"}}')->nullable();
            $table->char('contentSlug', 100)->comment('is alphabet')->nullable();
            $table->enum('contentIsParent', ['1', '0'])->comment('1 (true) or 0 (false)')->nullable();
            $table->integer('contentParentId')->comment('refer post id')->nullable();
            $table->string('contentStorageUrl')->comment('storage url')->nullable();
            $table->enum('contentStatus', ['publish', 'draft', 'bin'])->comment('status publish')->nullable();
            $table->smallInteger('contentPermissions')->comment('Everyone can read, write to, or execute | 700 Only you can read, write to, or execute | 744 Only you can read, write to, or execute but Everyone can read | 444 You can only read, as everyone else')->default('755');
            $table->char('contentScope', 50)->comment('photography', 'design', 'development', 'others')->nullable();
            $table->timestampTz('contentCreatedDate')->useCurrent();
            $table->timestampTz('contentUpdatedDate')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletesTz('contentDeletedDate')->nullable();
            $table->index([
                'contentSlug',
                'contentId'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
