<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer', function (Blueprint $table) {
            $table->increments('answerId');
            $table->integer('answerScore')->comment('score')->nullable();
            $table->json('answerValue')->comment('{"id": {"description": "description","title": "penjelasan"}, "en": {"description": "description","title": "penjelasan"}}')->nullable();
            $table->char('answerUserEmail', 100)->comment('is alphabet')->nullable();
            $table->enum('answerUserRole', ['ui/ux', 'front-end', 'bin'])->comment('status publish')->nullable();
            $table->char('answerSlug', 100)->comment('is alphabet')->nullable();
            $table->enum('answerIsParent', ['1', '0'])->comment('1 (true) or 0 (false)')->nullable();
            $table->integer('answerParentId')->comment('refer post id')->nullable();
            $table->string('answerStorageUrl')->comment('storage url')->nullable();
            $table->enum('answerStatus', ['publish', 'draft', 'bin'])->comment('status publish')->nullable();
            $table->smallInteger('answerPermissions')->comment('Everyone can read, write to, or execute | 700 Only you can read, write to, or execute | 744 Only you can read, write to, or execute but Everyone can read | 444 You can only read, as everyone else')->default('755');
            $table->char('answerScope', 50)->comment('trivial test', 'basic test', 'quiz')->nullable();
            $table->timestampTz('answerCreatedDate')->useCurrent();
            $table->timestampTz('answerUpdatedDate')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletesTz('answerDeletedDate')->nullable();
            $table->index([
                'answerSlug',
                'answerId'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer');
    }
}
