<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {
            $table->increments('questionId');
            $table->integer('questionCode')->comment('1111')->nullable();
            $table->integer('questionScore')->comment('100')->nullable();
            $table->char('questionCategory', 50)->comment('art', 'technology', 'history', 'geography')->nullable();
            $table->char('questionRole', 50)->comment('photography', 'ui/ux', 'front-end developer', 'back-end developer')->nullable();
            $table->char('questionTheAnswer', 100)->comment('A,B,C,D')->nullable();
            $table->json('questionValue')->comment('{"id": {"description": "description","title": "penjelasan"}, "en": {"description": "description","title": "penjelasan"}}')->nullable();
            $table->char('questionSlug', 100)->comment('is alphabet')->nullable();
            $table->enum('questionIsParent', ['1', '0'])->comment('1 (true) or 0 (false)')->nullable();
            $table->integer('questionParentId')->comment('refer post id')->nullable();
            $table->string('questionStorageUrl')->comment('storage url')->nullable();
            $table->enum('questionStatus', ['publish', 'draft', 'bin'])->comment('status publish')->nullable();
            $table->smallInteger('questionPermissions')->comment('Everyone can read, write to, or execute | 700 Only you can read, write to, or execute | 744 Only you can read, write to, or execute but Everyone can read | 444 You can only read, as everyone else')->default('755');
            $table->char('questionScope', 50)->comment('multiple choice', 'paragraph', 'boolean')->nullable();
            $table->timestampTz('questionCreatedDate')->useCurrent();
            $table->timestampTz('questionUpdatedDate')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->softDeletesTz('questionDeletedDate')->nullable();
            $table->index([
                'questionSlug',
                'questionId'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question');
    }
}
