<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Empty param or uri
 * @return not found
 */
Route::patch('/', function () {
    return response()->json([
        'status'  => 'error',
        'code'    => 404,
        'message' => 'url not found',
        'data'    => null
    ]);
});
Route::post('/', function () {
    return response()->json([
        'status'  => 'error',
        'code'    => 404,
        'message' => 'url not found',
        'data'    => null
    ]);
});
Route::put('/', function () {
    return response()->json([
        'status'  => 'error',
        'code'    => 404,
        'message' => 'url not found',
        'data'    => null
    ]);
});
Route::delete('/', function () {
    return response()->json([
        'status'  => 'error',
        'code'    => 404,
        'message' => 'url not found',
        'data'    => null
    ]);
});

Route::get('/', function () {
    return response()->json([
        'status'  => 'error',
        'code'    => 404,
        'message' => 'url not found',
        'data'    => null
    ]);
});


Route::group(
    [
        'prefix' => 'v1'
    ],
    function ($router) {
        $router->group(
            [
                'prefix'     => 'question',
                'middleware' => 'App\Http\Middleware\QuestionMiddleware'
            ],
            function ($routerQuestion) {
                /*
                | Create a message instance.
                |
                | @param uri
                */
                $routerQuestion->post('/', [
                    'uses'             =>  'QuestionController@post',
                    'as'               =>  'postQuestion'
                ]);
                $routerQuestion->get('/', [
                    'uses'             =>  'QuestionController@get',
                    'as'               =>  'getQuestion'
                ]);
            }
        );

        $router->group(
            [
                'middleware' => 'App\Http\Middleware\AnswerMiddleware'
            ],
            function ($routerAnswer) {
                /*
                | Create a message instance.
                |
                | @param uri
                */
                $routerAnswer->post('/answer', [
                    'uses'             =>  'AnswerController@post',
                    'as'               =>  'postAnswer'
                ]);
                $routerAnswer->get('/answer', [
                    'uses'             =>  'AnswerController@get',
                    'as'               =>  'getAnswer'
                ]);
            }
        );
    }
);
