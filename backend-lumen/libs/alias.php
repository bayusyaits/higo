<?php

function aliasResponseChilds($data = [], $param = '', $parentId = '', $lang = '') {
	$child = [];
	$scope    = ['content', 'answer', 'question'];
	if (isset($data) && isset($data[0])
	&& !empty($param) && in_array($param, $scope)
	) {
		$child['children'] = [];
		foreach ($data as $key => $value) {
			if (isset($value[$param . 'ParentId'])
				&& $value[$param . 'ParentId'] === $parentId) {
				array_push($child['children'], aliasResponseValue(
					$value,
					$param,
					'all',
					$lang,
					'',
					false));
			}
		}
		if (count($child['children']) >= 0 && isset($child['children'][0]['name'])) {
			return $child;
		}
	}
}

function aliasResponseValue($data = [], $param = '', $get = 'one', $lang = '', $pagination = [], $fields = true, $request = [])
{
	$content  = [];
	$scope    = ['content', 'answer',  'question'];
	$response = [];
	$tmpResponse = [];
	$mutableResponse = [];
	if (isset($data) && isset($data[0]) && $get == 'all') {
		foreach ($data as $key => $value) {
			# code...
			if (
			!empty($param)
			&& isset($value[$param . 'Id'])
			&& isset($value[$param . 'IsParent'])
			&& !empty($value[$param . 'IsParent'])) {
				$response[$key] = aliasResponseChilds(
						$data,
						$param,
						$value[$param . 'Id'],
						$lang);
			} else if (isset($request['parent'])
				&& !empty($value[$param . 'ParentId'])) {
				continue;
			} else if (isset($request['parent-backoffice'])
			&& !empty($value[$param . 'ParentId'])
			&& strpos($value[$param . 'Slug'], 'backoffice')) {
				break;
			}
			if (
				!empty($param)
				&& in_array($param, $scope)
				&& $value
				&& isset($value[$param . 'Value'])
			) {
				$content = $value[$param . 'Value'];
				$content = !requestHasInvalidJSON($content)
				? json_decode($content, true) : $content;
			} else {
				$content = $value;
			}

			if (
				isset($content) && isset($content['meta'])
				&& isset($content['meta']['en']) && $lang === 'en'
			) {
				$response[$key]['meta'] = $content['meta']['en'];
			} else if (
				isset($content) && isset($content['meta'])
				&& isset($content['meta']['id']) && $lang === 'id'
			) {
				$response[$key]['meta'] = $content['meta']['id'];
			} else if (isset($content) && isset($content['meta'])) {
				$response[$key]['meta'] = $content['meta'];
			}

			if (isset($content) && isset($content['detail'])) {
				$response[$key]['detail'] = $content['detail'];
			}

			if (isset($content) && isset($content['detail'])) {
				$response[$key]['detail'] = $content['detail'];
			}

			if (isset($content) && isset($content['globalParam'])) {
				$response[$key]['globalParam'] = $content['globalParam'];
			}
			if (!empty($param)
			&& $param === 'answer') {
				$response[$key]['email']		 	 = isset($value[$param . 'Email'])
				? $value[$param . 'Email'] : null;
			}

			if (
				!empty($param)
				&& in_array($param, $scope)
				&& isset($content['user'])
			) {
				$response[$key]['user'] = $content['user'];
			}

			if (
				isset($content['storage'])
				&& is_array($content['storage'])
			) {
				$getStorage = 'all';
			} else {
				$getStorage = 'one';
			}
			array_push($tmpResponse, $response[$key]);
		}
		$response = $tmpResponse;
	} else if (isset($data) && !empty($data)) {
		# code...
		if (
			!empty($param)
			&& in_array($param, $scope)
			&& $data && isset($data[0])
			&& isset($data[0][$param . 'Value'])
		) {
			$content = $data[0][$param . 'Value'];
		} else if (
			!empty($param)
			&& in_array($param, $scope)
			&& $data && isset($data[$param . 'Value'])
		) {
			$content = $data[$param . 'Value'];
			$content = !requestHasInvalidJSON($content)
			? json_decode($content, true) : $content;
		} else {
			$content = $data;
		}

		if (
			isset($content) && isset($content['meta'])
			&& isset($content['meta']['en']) && $lang === 'en'
		) {
			$response['meta'] = $content['meta']['en'];
		} else if (
			isset($content) && isset($content['meta'])
			&& isset($content['meta']['id']) && $lang === 'id'
		) {
			$response['meta'] = $content['meta']['id'];
		} else if (isset($content) && isset($content['meta'])) {
			$response['meta'] = $content['meta'];
		}

		if (isset($content) && isset($content['detail'])) {
			$response['detail'] = $content['detail'];
		}

		if (isset($content) && isset($content['globalParam'])) {
			$response['globalParam'] = $content['globalParam'];
		}

		if (!empty($param)
		&& $param === 'answer') {
			$response['email']		 	 = isset($data[0][$param . 'Email'])
			? $data[0][$param . 'Email'] : $data[$param . 'Email'];
		}

		if (
			!empty($param)
			&& in_array($param, $scope)
			&& isset($content['user'])
		) {
			$response['user'] = $content['user'];
		}

		if (
			isset($content['storage'])
			&& is_array($content['storage'])
		) {
			$getStorage = 'all';
		} else {
			$getStorage = 'one';
		}

	} else {
		$response = '';
	}

	if ($fields) {
		$mutableResponse['fields'] = $response;
	} else {
		$mutableResponse = $response;
	}

	if (isset($pagination['total']) && isset($pagination['limit'])
	&& $pagination['limit'] > count($response)) {
		$pagination['total'] = strval(count($response));
	}

	if (isset($pagination['total'])) {
		$mutableResponse['pagination'] = $pagination;
	}

	return $mutableResponse;
}
