<?php

/*
* use crypto for php
* $encrypted = cryptoJsAesEncrypt("my passphrase", "value to encrypt");
* $decrypted = cryptoJsAesDecrypt("my passphrase", $encrypted);
*/

function baseUrl($slug = false)
{
	if ($slug) {
		if (strpos($slug, "backoffice")) {
			return route("backoffice");
		}
	}
}
function _post($name, $default = null)
{
	return isset($_POST[$name]) && $_POST[$name] ? $_POST[$name] : $default;
}
function _get($name, $default = null)
{
	return isset($_GET[$name]) && $_GET[$name] ? $_GET[$name] : $default;
}
function _server($name, $default = null)
{
	return isset($_SERVER[$name]) && $_SERVER[$name] ? $_SERVER[$name] : $default;
}
function _session($name, $default = null)
{
	return isset($_SESSION[$name]) && $_SESSION[$name] ? $_SESSION[$name] : $default;
}
function _cookie($name, $default = null)
{
	return isset($_COOKIE[$name]) && $_COOKIE[$name] ? $_COOKIE[$name] : $default;
}

function fileDimensions($file)
{
	return list($width, $height) = getimagesize("$file");
}

function removeExt($string)
{
	return preg_replace('/\\.[^.\\s]{3,4}$/', '', $string);
}

function getRequestHeaders()
{
	$headers = array();
	foreach ($_SERVER as $key => $value) {
		if (substr($key, 0, 5) <> 'HTTP_') {
			continue;
		}
		$header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
		$headers[$header] = $value;
	}
	return $headers;
}

function getClientIp()
{
	$ipaddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP']))
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if (isset($_SERVER['HTTP_X_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	else if (isset($_SERVER['HTTP_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	else if (isset($_SERVER['REMOTE_ADDR']))
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipaddress = '0';
	return $ipaddress;
}

function convertToObject($array)
{
	$object = new stdClass();
	foreach ($array as $key => $value) {
		if (is_array($value)) {
			$value = convertToObject($value);
		}
		$object->$key = $value;
	}
	return $object;
}

function initUserType($int) {
	$data = '';
	if (isset($int) && $int) {
		$data = intval($int);
	} else {
		return '';
	}

	if ($data === 1) {
		$data = 'fo';
	}
	else if ($data === 2) {
		$data = 'bo';
	}

	return $data;
}

function flattenArray(array $array)
{
	$return = array();
	array_walk_recursive($array, function ($a) use (&$return) {
		$return[] = $a;
	});
	return $return;
}

function flattenArrayStorage($array, $for = 'storage')
{
	$data = [];
	$print = [];
	$scopePermissions = [
		700,
		744,
		444,
		755
	];
	if (is_array($array) && isset($array) && $for === 'storage') {
		$data = flattenArray($array);
		if (is_array($data) && in_array(true, $data)) {
			foreach($data as $value) {
				if ($value
				&& filter_var($value, FILTER_VALIDATE_URL)) {
					$print['fileUrl'] = $value;
				}
				if ($value && validateFileType($value)) {
					$print['fileType'] = $value;
				}
				if ($value && preg_match('/^[a-z0-9.]+$/i', $value)) {
					$print['fileSlug'] = preg_replace("/[\ _]/", "-", $value);
					$print['fileName'] = $value;
				}
				if ($value && $value === true) {
					$print['isParent'] = $value;
				}
				if ($value && is_numeric($value)
				&& in_array((float)($value), $scopePermissions)) {
					$num = (float)($value);
					$print['filePermissions'] = $num;
				}
			}
		}
	} else if (isset($array) && !empty($array) && $for === 'storage') {
		$print = $array;
	}
	return $print;
}

function convertArray($array, $for = 'value')
{
	$obj = $array;
	if (is_array($array)) {
		foreach ($array as $key => $value) {
			$obj = $for === 'key' ? $key : $value;
		}
	}
	return $obj;
}

function isValidDomain($domain)
{
	return preg_match("/^(?!\-)(?:(?:[a-zA-Z\d][a-zA-Z\d\-]{0,61})?[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/", $domain);
}

function convertArrayParam($array, $scope = '')
{
	$bool = false;
	if (is_array($array)) {
		foreach ($array as $key => $value) {
			if (is_array($value) && !isset($value['param'])) {
				$value = convertArrayParam($value);
			}

			if (isset($value['param']) && $value['param'] === $scope) {
				$bool = true;
			}
		}
	}
	return $bool;
}

function setAttribute($value)
{
	$properties = [];

	foreach ($value as $array_item) {
		if (!is_null($array_item['value'])) {
			$properties[] = $array_item;
		}
	}

	return json_encode($properties);
}

function cleanCharsAndSpace($string)
{
	$search = array('/[^A-Za-z0-9\-]/', '_');
	$replace = array(' ', ' ');
	$subject = $string;
	return str_replace($search, $replace, $subject);
}

function requestHasInvalidJSON($data)
{
	if ($data && is_array($data)) {
		return true;
	} else if (!$data || empty($data)) {
		return true;
	}
	json_decode($data);
	return json_last_error() !== JSON_ERROR_NONE;
}

function generateToken($length = null)
{
	if (empty($length)) {
		$length = 20;
	} else {
		$length = $length;
	}
	$buf = '';
	for ($i = 0; $i < $length; ++$i) {
		$buf .= chr(mt_rand(0, 255));
	}
	return bin2hex($buf);
}

function generateRandomNumeric($length = null)
{
	if (empty($length)) {
		$length = 10;
	} else {
		$length = $length;
	}
	$numeric = '0123456789';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $numeric[rand(0, strlen($numeric) - 1)];
	}
	return $randomString;
}

function base64Image($path)
{
	$type = pathinfo($path, PATHINFO_EXTENSION);
	$data = file_get_contents($path);
	return 'data:image/' . $type . ';base64,' . base64_encode($data);
}

function generateRandomString($length = null)
{
	if (empty($length)) {
		$length = 10;
	} else {
		$length = $length;
	}
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}

function generateRandomCode($length = null)
{
	if (empty($length)) {
		$length = 6;
	} else {
		$length = $length;
	}
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}
/**
 * renameResponseField('postContent', 'postContentSlugScope')
 * @result scope
 */
function renameResponseField($param = '', $data = '')
{
	if (is_array($data) || $param === $data) {
		return $data;
	}
	return lcfirst(str_replace($param, '', $data));
}

function removesSpecialChars($string)
{
	$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

	return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

function createHash($stored)
{
	$hash = password_hash(base64_encode(hash('sha256', $stored, true)), PASSWORD_DEFAULT);
	return $hash;
}

function validateHash($stored, $hash_bcrypt)
{
	$hash = $hash_bcrypt;
	if (password_verify(base64_encode(hash('sha256', $stored, true)), $hash_bcrypt))
		return true;
}

function validateClientScope($scope = [], $clientScope = [])
{
	$explode1 = explode(',', $scope);
	$explode2 = explode(',', $clientScope);
	$array	 	= [];
	if (is_array($explode1) && is_array($explode2)) {
		$val = array_intersect($explode1, $explode2);
	} else if (is_array($explode1) && !is_array($explode2)) {
		array_push($array, $clientScope);
		$val = array_intersect($explode1, $array);
	} else if (!is_array($explode1) && is_array($explode2)) {
		array_push($array, $scope);
		$val = array_intersect($array, $explode2);
	} else if ($scope === $clientScope) {
		$val = $scope;
	} else {
		$val = $clientScope;
	}
	return $val;
}

function validateFileType($file = '')
{
	$bool = true;
	$arr = [
		'audio/aac',
		'audio/mpeg',
		'audio/webm',
		'video/webm',
		'video/mpeg',
		'image/gif',
		'image/jpg',
		'image/png',
		'image/jpeg',
		'image/svg+xml',
		'application/pdf',
		'application/vnd.ms-powerpoint',
		'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'application/vnd.ms-word',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'application/vnd.ms-excel',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
	];

	if (is_array($file) || empty($file) || !in_array($file, $arr)) {
		$bool = false;
	}
	return $bool;
}

function getRomawi($month = 1)
{
	$arr = array(
		1 => 'I', 'II', 'III', 'IV', 'V',
		'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII'
	);
	return $arr[$month];
}
/**
 * Format daate DD-MM-YYYY
 */
function dateToEn($date)
{
	$date_en 	 = date("Y-m-d H:i:s", $date);
	$date_i 	 = date('Y-m-d', strtotime($date_en));
	$month_array = date('m', strtotime($date_en));
	$dateObj   	 = DateTime::createFromFormat('!m', $month_array);
	$month 		 = $dateObj->format('F');
	$date 		 = date('d', strtotime($date_en));
	$year 		 = substr($date_en, 0, 4);
	$monthName 	 = substr($month, 0, 3);
	if ($date_i == '1970-01-01') {
		return '';
	} else {
		return $monthName . " " . $date . ", " . $year;
	}
}

/**
 * Format daate DD-MM-YYYY
 */
function dateToTime($date)
{
	$time = date('Y-m-d H:i:s', $date);
	return $time;
} //function_date_to_en

/**
 * Format daate DD-MM-YYYY
 */
function dateToId($date)
{
	$date_id 	 = date("Y-m-d H:i:s", $date);
	$date_i 	 = date('Y-m-d', strtotime($date_id));
	$month_array = date('m', strtotime($date_id));
	$bulan 		 = array(
		"",
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember"
	);
	$date 		 = date('d', strtotime($date_id));
	$year 		 = substr($date_id, 0, 4);
	$month_array = $month_array < 10 ? substr($month_array, 1, 1) : $month_array;
	if ($date_i == '1970-01-01') {
		return '';
	} else {
		return $date . " " . $bulan[$month_array] . " " . $year;
	}
}

function timeId()
{
	date_default_timezone_set('Asia/Jakarta');
	return time();
}

/**
 * Format daate DD-MM-YYYY
 */
function dateTimeId($date)
{
	$date_id 		= date("Y-m-d H:i:s", $date);
	$dates 			= new DateTime($date_id, new DateTimeZone('Asia/Jakarta'));
	$date_i			= $dates->format('Y-m-d H:i:s');
	if ($date_i == '1970-01-01') {
		return '';
	} else {
		return $date_i;
	}
}

function dateTimeMilisecond($date)
{
	$val = strtotime($date);
	return $val;

}

/**
 * Format daate DD-MM-YYYY
 */
function dateTimeEn($date)
{
	$date_id 		= date("Y-m-d H:i:s", $date);
	$month_array 	= date('m', strtotime($date_id));
	$dateObj   		= DateTime::createFromFormat('!m', $month_array);
	$month 			= $dateObj->format('F');
	$monthName 		= substr($month, 0, 3);
	$date 			= date('d', strtotime($date_id));
	$newDateTime 	= date('h:i A', strtotime($date_id));
	$year 			= substr($date_id, 0, 4);
	$date_i 		= $monthName . " " . $date . ", " . $year . " at " . $newDateTime . "";
	if ($date_i == '1970-01-01') {
		return '';
	} else {
		return $date_i;
	}
}

function sanitizePostXss($input)
{

	$data  = isset($input) ? $input : $_POST;
	foreach ($data as $key => $value) {
		if (is_array($value)) {
			$data[$key] = nestedXssFilter($value);
		} else {
			$data[$key] = cleanXss($value);
		}
	}

	if (!isset($input)) {
		$_POST = $data;
	}

	$input = $data;
	return $data;
}

function nestedXssFilter(&$data = [])
{
	foreach ($data as $key => $value) {
		if (is_array($value)) {
			$data[$key] = nestedXssFilter($data[$key]);
		} else {
			$data[$key] = cleanXss($value);
		}
	}

	return $data;
}

function cleanXss($string)
{
	if (!requestHasInvalidJSON($string)) {
		return $string;
	}
	$string = preg_replace('/(<[a-zA-Z]+)|(<\/[a-zA-Z]+>)|>/', '', $string);
	return htmlspecialchars(strip_tags($string));
}


function cleanSlugName($data = '')
{
	if (is_array($data)) {
		return $data;
	}
	return strtolower(str_replace(' ', '-', $data));
}

function jsonEncode($input = '')
{
	if (empty($input)) {
		return '';
	}
	return json_encode($input, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS);
}

function controller($module = null, $class = '', $object = true)
{
	$src = "\App\Http\Controllers\\$class";
	if (!empty($module) && $module) {
		$src = "\Modules\\$module\Http\Controllers\\$class";
	}
	if ($object) {
		return new $src(true);
	}
	return $src;
}

function entity($module = '', $class = '')
{
	$src = "\App\Models\\$class";
	if (!empty($module)) {
		$src = "\Modules\\$module\Models\\$class";
	}

	if (!class_exists($src)) {
		return 'not found';
	}

	return $src;
}

function module($module = '')
{
	$src = "\Modules\\$module";

	if (!class_exists($src))
		return 'not found';

	return new $src;
}

function explodeSearch($keywords = [])
{
	$explode = explode(',', $keywords);
	foreach ($explode as $key => $value) {
		# code...
		if (isset($value[$key])) {
			$explode[$key] = '%' . $value . '%';
		}
	}
	return $explode;
}

function removeHttp($url)
{
	$disallowed = array('http://', 'https://');
	foreach ($disallowed as $d) {
		if (strpos($url, $d) === 0) {
			return str_replace($d, '', $url);
		}
	}
	return $url;
}

function validateHttp($url)
{
	$bool = false;
	$disallowed = array('http://', 'https://');
	foreach ($disallowed as $d) {
		if (strpos($url, $d) === 0) {
			$bool = true;
		}
	}
	return $bool;
}

function sharingPermissions($number, $search) {
	$arr = [
		700 => [
			'me'				=> 'write',
			'group'			=> 'read',
			'everyone'	=> 'read'
		],
		744 => [
			'me'				=> 'read,write',
			'group'			=> 'read',
			'everyone'	=> 'read'
		],
		444 => [
			'me'				=> 'read',
			'group'			=> 'read',
			'everyone'	=> null
		],
		755	=> [
			'me'				=> 'read,write',
			'group'			=> 'read,write',
			'everyone'	=> 'read,write'
		]
	];

	if (!isset($number) || empty($number)) {
		return [];
	}

	$number = (float)$number;

	if (!in_array($number, array_keys($arr))) {
		return array_keys($arr);
	}

	if (!isset($search) || empty($search)) {
		return $arr[$number];
	}

	return array_search($search, $arr[$number]);
}

function accessRole($userRole, $accessRole, $permissions) {
	$bool = false;
	if (!isset($userId) || !isset($permissions)
	|| !isset($accessRole) || !is_array($accessRole)) {
		return $bool;
	}

	if (in_array($userRole, $accessRole)) {
		$bool = true;
	}

	return $bool;
}

function explodeUserData($param, $scope = 'userRole') {
	$explode = explode('||', $param);
	$str = null;
	if (is_array($explode)) {
		if ($scope === 'userRole' && isset($explode[3])
		&&  !empty($explode[3])) {
			$str = $explode[3];
		}
		if ($scope === 'userType' && isset($explode[4])
		&&  !empty($explode[4])) {
			$str = $explode[4];
		}
		if ($scope === 'userStatus' && isset($explode[5])
		&&  !empty($explode[5])) {
			$str = $explode[5];
		}
	}
	return $str;
}


function renameParam($param, $scope = ['user']) {
	$str = $param;
	if (strpos($str, $scope[0]) === 0) {
		$str = lcfirst(substr($str, strlen($scope[0])));
	} else if (strpos($str, $scope[1]) === 0) {
		$str = lcfirst(substr($str, strlen($scope[1])));
	}
	return $str;
}


function scopeUserRole($userRole, $scope = 'write') {
	$scopePrivilage = ['read', 'write', 'execute'];
	$scopeUser = ['admin', 'contributor', 'content_manager'];
	$bool = false;
	if (!isset($userRole) && !isset($scope)) {
		return $bool;
	} else if (!in_array($scope, $scopePrivilage)) {
		return $bool;
	}
	if (in_array($userRole, $scopeUser)
	&& in_array($scope, $scopePrivilage)) {
		$bool = true;
	}
	return $bool;
}

function checkParamUserToWrite($param) {
	$bool = [];
	$scope = ['userId', 'userRole', 'userStatus'];
	if (is_array($param)) {
		foreach ($scope as $key => $value) {
			$bool[$key] = false;
			if (in_array($value, array_keys($param))) {
				$bool[$key] = !empty($param[$value]) ? true : false;
			}
		}
		if (is_array($bool) &&
		in_array(false, $bool)) {
			$bool = false;
		} else {
			$bool = true;
		}
	} else {
		$bool = false;
	}
	return $bool;
}
