<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Validator;
use Throwable;

class AnswerController extends Controller
{
    /**
     * Create a new AnswerController instance.
     *
     * @return void
     */
    private $scope;

    public function __construct($scope = ['content'])
    {
        $this->scope = $scope;
    }

    public function post(Request $request, $uri = null)
    {
        $input     = $request->all();
        $input     = sanitizePostXss($input);
        $userData  = $request->auth;
        $validator = Validator::make($input, [
            'value'       => 'required',
            'status'      => '',
            'permissions' => ''
        ]);

        if ($validator->fails()) {
            //return error message
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_FORBIDDEN,
                'message' => 'validation is fail',
                'data'    => null
            ]);
        }

        if (isset($input['value'])
            && requestHasInvalidJSON($input['value'])
        ) {
            //return error message
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_FORBIDDEN,
                'message' => 'value is not valid',
                'data'    => null
            ]);
        }

        $input['listScope']  = $this->scope;
        $input['scope']  = $uri;
        $input['lang']   = isset($request->lang) && !empty($request->lang)
            ? $request->lang : 'all';

        try {
            $insertData = entity(null, 'AnswerModel')::insertAnswer($input, 'unique');
        } catch (Throwable $e) {
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_FORBIDDEN,
                'message' => 'forbidden to add data',
                'data'    => null
            ]);
        }

        if (is_array($insertData)) {
            return response()->json($insertData);
        }

        /**
         * If value not array
         */
        return response()->json([
            'status'  => 'error',
            'code'    => Response::HTTP_INTERNAL_SERVER_ERROR,
            'message' => 'internal server error',
            'data'    => env('APP_ENV') !== 'production' ? $insertData : null
        ]);
    }

    public function put(Request $request, $uri = null)
    {
        $input     = $request->all();
        $input     = sanitizePostXss($input);
        $validator = Validator::make($input, [
            'value'       => 'required',
            'status'      => '',
            'permissions' => ''
        ]);

        if ($validator->fails()) {
            //return error message
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_FORBIDDEN,
                'message' => 'validation is fail',
                'data'    => null
            ]);
        }

        if (isset($input['value']) && requestHasInvalidJSON($input['value'])) {
            //return error message
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_FORBIDDEN,
                'message' => 'value is not valid',
                'data'    => null
            ]);
        }

        if (!empty($uri)
            && !in_array($uri, $this->scope)) {
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_NOT_FOUND,
                'message' => 'url not found',
                'data'    => null
            ]);
        }

        $input['listScope']  = $this->scope;
        $input['scope']  = $uri;
        $input['lang']   = isset($request->lang) && !empty($request->lang)
            ? $request->lang : 'all';

        try {
            $editData = entity(null, 'AnswerModel')
                ::updateAnswer($input);
        } catch (Throwable $e) {
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_FORBIDDEN,
                'message' => 'forbidden to edit data',
                'data'    => null
            ]);
        }

        if (is_array($editData)) {
            return response()->json($editData);
        }

        /**
         * If value not array
         */
        return response()->json([
            'status'  => 'error',
            'code'    => Response::HTTP_INTERNAL_SERVER_ERROR,
            'message' => 'internal server error',
            'data'    => env('APP_ENV') !== 'production' ? $editData : null
        ]);
    }

    public function delete(Request $request, $uri = null, $slug = null)
    {
        $input     = [];
        if (!isset($request->token)
        ) {
            //return error message
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_FORBIDDEN,
                'message' => 'validation is fail',
                'data'    => null
            ]);
        }


        if (!in_array($uri, $this->scope)) {
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_NOT_FOUND,
                'message' => 'url not found',
                'data'    => null
            ]);
        }

        $getData  = entity(null, 'AnswerModel')
            ::getAnswer(['slug' => $slug]);
        if (count($getData->get()) === 0) {
            //return error message
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_NOT_FOUND,
                'message' => '[151203031] data not found',
                'data'    => null
            ]);
        }
        $now    = \Carbon\Carbon::now();
        setlocale(LC_TIME, 'IND');

        try {
            $editAnswer = $getData->update([
                'answerDeletedDate'  => $now
            ]);
        } catch (Throwable $e) {
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_FORBIDDEN,
                'message' => 'forbidden to delete data',
                'data'    => null
            ]);
        }
        //return successful response
        return response()->json([
            'status'  => 'success',
            'code'    => Response::HTTP_OK,
            'message' => 'data was deleted',
            'data'    => aliasResponseValue($getData->first())
        ]);
    }

    /**
     * List related with name or ms
     * Related separated with table post, because consideration for use api public
     */

    public function get(Request $request, $uri = null)
    {
        $param       = $request->all();
        $getRelate   = null;
        $get         = isset($param['get']) && !empty($param['get']) ? $param['get'] : 'one';
        $lang        = isset($request->lang) && !empty($request->lang)
            ? $request->lang : 'all';
        $pagination  = [];

        if (!empty($uri) && !in_array($uri, $this->scope)) {
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_NOT_FOUND,
                'message' => 'url not found',
                'data'    => null
            ]);
        }
        $param['scope']  = $uri;

        $getData  = entity(null, 'AnswerModel')
            ::getAnswer($param);

        if ($get === 'all') {
            if (isset($param['sortBy']) && $param['sortBy'] === 'id') {
                $getData = $getData->orderBy('answerId', 'desc');
            }

            if (isset($param['currentPage']) && isset($param['limit'])) {
                $total   = strval(count($getData->get()));
                $currentPage = $param['currentPage'] > 0
                ? $param['limit'] * $param['currentPage']
                : $param['currentPage'];
                $getData = $getData->skip($currentPage)
                    ->take($param['limit']);
                $pagination = [
                    'total'         => $total,
                    'currentPage'   => $param['currentPage'],
                    'limit'         => $param['limit']
                ];
            }
            $getData = $getData->get();

            if (count($getData) !== 0) {

                return response()->json([
                    'status'  => 'success',
                    'code'    => Response::HTTP_OK,
                    'message' => 'access data is success',
                    'data'    => aliasResponseValue(
                        $getData,
                        'answer',
                        $get,
                        $lang,
                        $pagination,
                        false
                    )
                ]);
            }
        } else {
            $getData = $getData->first();

            if (!empty($getData)) {
                return response()->json([
                    'status'  => 'success',
                    'code'    => Response::HTTP_OK,
                    'message' => 'access data is success',
                    'data'    => aliasResponseValue(
                        $getData,
                        'answer',
                        $get,
                        $lang,
                        $pagination
                    )
                ]);
            }
        }

        return response()->json([
            'status'  => 'error',
            'code'    => Response::HTTP_NOT_FOUND,
            'message' => '[151203032] data not found',
            'data'    => null
        ]);
    }
}
