<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class QuestionController extends Controller
{
/**
     *
     * @return void
     */
    private $scope;

    public function __construct($scope = ['content'])
    {
        $this->scope = $scope;
    }

    public function post(Request $request, $uri = null)
    {
        return response()->json([
            'status'  => 'error',
            'code'    => Response::HTTP_NOT_FOUND,
            'message' => 'get not found',
            'data'    => null
        ]);
    }

    public function get(Request $request, $uri = null)
    {
        return response()->json([
            'status'  => 'error',
            'code'    => Response::HTTP_NOT_FOUND,
            'message' => 'get not found',
            'data'    => null
        ]);
    }
}
