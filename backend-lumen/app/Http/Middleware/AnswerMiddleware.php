<?php

namespace App\Http\Middleware;

use Closure;
use Throwable;
use MsAuth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redis;

class AnswerMiddleware
{
    public function handle(Request $request, Closure $next, $guard = null)
    {
        $ip      = getClientIp();
        $domain  = '';
        $headers = getRequestHeaders();

        if (isset($headers["Origin"])) {
            $domain = removeHttp($headers["Origin"]);
        } elseif (isset($headers["Host"])) {
            $domain = $headers["Host"];
        }

        if (empty($ip) || empty($domain)) {
            // Unauthorized response if token not there
            return response()->json([
                'status'  => 'error',
                'code'    => Response::HTTP_FORBIDDEN,
                'message' => 'forbidden access',
                'data'    => null
            ]);
        }

        return $next($request);
    }
}
