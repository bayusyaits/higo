<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {

        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($this->isHttpException($exception)) {
            switch ($exception->getStatusCode()) {
                    // not authorized
                case '403':
                    return response()->json([
                        'status'  => 'error',
                        'code'    => 403,
                        'message' => '[071] forbidden access',
                        'data'    => null
                    ]);
                    break;

                    // not found
                case '404':
                    return response()->json([
                        'status'  => 'error',
                        'code'    => 404,
                        'message' => '[072] not found',
                        'data'    => null
                    ]);
                    break;

                    // internal error
                case '500':
                    return response()->json([
                        'status'  => 'error',
                        'code'    => 500,
                        'message' => '[073] internal server error',
                        'data'    => null
                    ]);
                    break;

                case '405':
                    return response()->json([
                        'status'  => 'error',
                        'code'    => 405,
                        'message' => '[074] method not allowed',
                        'data'    => null
                    ]);
                    break;

                default:
                    return response()->json([
                        'status'  => 'error',
                        'code'    => $exception->getStatusCode(),
                        'message' => '[071] '.$exception->getMessage(),
                        'data'    => null
                    ]);
                    break;
            }
        }
        $exc = get_class($exception);
        if ($exception instanceof tokenInvalidException) {
            switch ($exc) {
                case OAuthServerException::class:
                    return response()->json(
                        [
                            'error' => $exception->getErrorType(),
                            'message' => '[076] '.$exception->getMessage(),
                            'hint' => $exception->getHint(),
                        ],
                        $exception->getHttpStatusCode()
                    );
                case TokenInvalidException::class:
                    return response()->json(
                        [
                            'error' => $exception->getErrorType(),
                            'message' => '[077] Token is invalid',
                            'hint' => $exception->getHint(),
                        ],
                        $exception->getHttpStatusCode()
                    );
                default:
                    return (parent::render($request, $exception));
            }
        } elseif (!in_array($exc, $this->dontReport)) {
            // do whatever
            return response()->json([
                'status'  => 'error',
                'code'    => 500,
                'message' => '[0710] internal server error',
                'data'    => env('APP_ENV') !== 'production' ? $exc : null
            ]);
        }
    }

}
