<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AnswerModel extends Model
{

    //
    protected static $elq = __CLASS__;
    protected $table      = 'answer';
    protected $primaryKey = 'answerId';
    protected $dates      = ['answerDeletedDate'];

    protected $casts = [
        'answerId'    => 'int',
        'answerValue' => 'array'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'answerValue',
        'answerStorageUrl',
        'answerScope',
        'answerSlug',
        'answerStatus',
        'answerPermissions',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;
    const CREATED_AT = 'answerCreatedDate';
    const UPDATED_AT = 'answerUpdatedDate';

    protected $fieldRules = [
        'answerSlug'       => ['',''],
        'answerValue'      => ['',''],
        'answerStorageUrl' => ['',''],
        'answerUserId'     => ['','']
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function scopeAddAnswer($query, $body = [])
    {
        $query->insert([
            'answerSlug'           =>  $body['slug'],
            'answerIsParent'       =>  isset($body['isParent'])
            ? $body['isParent'] : null,
            'answerParentId'       =>  isset($body['parentId'])
            ? $body['parentId'] : null,
            'answerUserEmail'       =>  isset($body['email'])
            ? $body['email'] : null,
            'answerUserRole'       =>  isset($body['role'])
            ? $body['role'] : null,
            'answerScore'       =>  isset($body['score'])
            ? $body['score'] : null,
            'answerStorageUrl'     =>  isset($body['storageUrl'])
            ? $body['storageUrl'] : null,
            'answerScope'          =>  $body['scope'],
            'answerValue'          =>  $body['value'],
            'answerStatus'         => isset($body['status'])
            ? $body['status'] : null,
            'answerPermissions'    => isset($body['permissions'])
            ? $body['permissions'] : '755'
        ]);
    }

    public function scopeGetContentJson($query, $body = [], $condition = '', $unionData = null)
    {
        $query->selectRaw('answerId,answerSlug,answerValue,answerUserEmail,answerScore,
        answerUserRole,answerScope,answerPermissions,answerStatus,answerUpdatedDate,
        answerCreatedDate,answerUserRole,answerIsParent,answerParentId');
        $explode = explode(',', $condition);
        $uri     = '->attr-uri';
        $result  = convertArrayParam($body, 'answerScope');

        if (isset($explode[1])) {
            foreach ($explode as $key => $value) {
                # code...
                $cond[$key] = $value;
            }
        } else {
            $cond = $condition;
        }

        if ($cond && !empty($cond)) {
            $query->whereNotNull($cond);
        }

        if ($result) {
            $uri = '->name';
        }

        if (isset($body[0])
        && isset($body[0]['param'])
        && isset($body[0]['value'])) {
            foreach ($body as $key => $value) {
                # code...
                $obj[$key] = $body[$key];
                if (!isset($body[$key]['param'])
                || !isset($body[$key]['value'])) {
                    return null;
                }
                if ($body[$key]['param'] === 'answerValue'
                && $uri === '->name') {
                    $body[$key]['value'] = str_replace('-', ' ', $body[$key]['value']);
                    $obj[$key]['param'] = $body[$key]['param'].$uri;
                } elseif ($body[$key]['param'] === 'answerValue') {
                    $obj[$key]['param'] = $body[$key]['param'].$uri;
                }
                $query->where([
                    [$obj[$key]['param'], '=', $obj[$key]['value']],
                    ['answerDeletedDate', '=', null]
                ]);
            }
        } else {
            $obj = $body;

            if (!isset($body['param'])
            || !isset($body['value'])) {
                return null;
            }
            if ($body['param'] === 'answerValue') {
                $obj['param'] = $body['param'].$uri;
            }
            $query->where($obj['param'], $obj['value']);
        }

        if (!empty($unionData)) {
            $query->union($unionData);
        }

        return $query;
    }

    public function scopeAddRandomSlugName($query, $scope = 'content', $length = 4)
    {
        $name = strtolower(generateRandomCode($length));
        $query->where('answerSlug', $name);
        if (!$query->first() || empty($query->first())) {
            return $name;
        }
        return $this->addRandomSlugName($scope, $length + 1);
    }

    public function scopeInsertanswer($query, $input = [], $validator = '')
    {
        $inputValue  = isset($input['value']) ? json_decode($input['value']) : null;
        $validate    = [];
        $scope       = isset($input['scope']) ? cleanSlugName($input['scope']) : null;
        $listScope   = isset($input['listScope']) ? $input['listScope'] : ['content'];
        $lang        = isset($input['lang']) && !empty($input['lang'])
        ? $input['lang'] : 'all';
        $body        = $input;
        $response    = [];
        $tmpResponse = [
            'status' => 'success',
            'code'   => 200,
            'message'=> 'crated data'
        ];

        if (is_array($inputValue)) {
            $inputValue = flattenArray($inputValue);
            foreach ($inputValue as $key => $value) {
                $body['scope']  = $scope;
                $body['value']  = $inputValue[$key];
                $metaName       = isset($body['value']->name)
                && !empty($body['value']->name)
                ? cleanSlugName($body['value']->name)
                : $this->addRandomSlugName($scope);

                if (empty($body['scope'])
                && isset($body['value']->scope)
                && !empty($body['value']->scope)) {
                    $body['scope'] = $body['value']->scope;
                } elseif (empty($body['scope'])
                && !isset($body['value']->scope)) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = '[] scope is required';
                } elseif (!in_array($body['scope'], $listScope)) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = '[] scope not valid';
                }

                $body['slug']   = $body['scope'].'-'.$metaName;

                if (isset($body['value']->status)
                && !empty($body['value']->status)) {
                    $body['status'] = $body['value']->status;
                } elseif (!isset($body['status'])
                || empty($body['status'])) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = 'status is required';
                }

                if (isset($body['value']->permissions)
                && !empty($body['value']->permissions)) {
                    $body['permissions'] = $body['value']->permissions;
                } elseif (!isset($body['permissions'])
                || empty($body['permissions'])) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = 'permissions is required';
                }

                if (isset($body['value']->input)
                && !empty($body['value']->input)) {
                    $body['input'] = $body['value']->input;
                } elseif (!isset($body['input'])
                || empty($body['input'])) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = 'input is required';
                }

                if (isset($body['value']->answer)
                && !empty($body['value']->answer)) {
                    $body['answer'] = $body['value']->answer;
                } elseif (!isset($body['answer'])
                || empty($body['answer'])) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = 'answer is required';
                }

                if (isset($body['value']->questions)
                && !empty($body['value']->questions)) {
                    $body['questions'] = $body['value']->questions;
                } elseif (!isset($body['questions'])
                || empty($body['questions'])) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = 'questions is required';
                }

                if (isset($body['input']->email)
                && !empty($body['input']->email)) {
                    $body['email'] = $body['input']->email;
                } elseif (!isset($body['email'])
                || empty($body['email'])) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = 'email is required';
                }

                if (isset($body['input']->role)
                && !empty($body['input']->role)) {
                    $body['role'] = $body['input']->role;
                } elseif (!isset($body['role'])
                || empty($body['role'])) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = 'role is required';
                }

                if (!isset($body['value']->isParent)
                && isset($body['isParent'])) {
                    unset($body['isParent']);
                } elseif (isset($body['value']->isParent)) {
                    $body['isParent'] = $body['value']->isParent;
                }

                if (isset($body['value']->parentName)
                && !isset($body['parentId'])
                && !empty($body['value']->parentName)) {
                    $body['isParentId'] = false;
                    $parentSlug   = $body['scope'].'-'.cleanSlugName($body['value']->parentName);

                    $getData     = $this->getanswer([
                        'slug'     => $parentSlug
                    ]);

                    if (count($getData->get()) === 0) {
                        //return error message
                        $tmpResponse['error'][$key] = true;
                        $tmpResponse['message']     = 'parent name not found';
                    }

                    $getData = $getData->first();

                    if (isset($getData->answerId)
                    && !empty($getData->answerId)) {
                        $body['isParentId'] = true;
                        $body['parentId'] = $getData->answerId;
                    } else {
                        //return error message
                        $tmpResponse['error'][$key] = true;
                        $tmpResponse['message']     = 'parent id not found';
                    }
                }

                if (!empty($validator) && $validator === 'unique') {
                    $validate = $this->validateanswer($body, 'value');
                }

                if (is_array($validate) && count($validate) !== 0) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = $body['slug']
                    .' has been created';
                }

                if (isset($tmpResponse['error'][$key])) {
                    $response[$key] = [
                        'status'  => 'error',
                        'code'    => 503,
                        'message' => $tmpResponse['message'],
                        'data'    => null
                    ];
                } else {
                    $tmpResponse['success'][$key]  = true;
                    $body['value']  = jsonEncode($inputValue[$key]);
                    $insertData     = entity(null, 'AnswerModel')
                    ::addAnswer($body);
                    $response[$key] = $insertData->where([
                        'answerSlug' => $body['slug']
                    ])->first();
                    $response[$key] = aliasResponseValue(
                        $response[$key],
                        'answer',
                        'one',
                        $lang
                    );
                }
                if (isset($body['parentId'])
                && isset($body['isParentId'])
                && $body['isParentId']) {
                    unset($body['parentId']);
                }
            }
        } else {
            $body['scope']  = $scope;
            $body['value']  = $inputValue;
            $metaName       = isset($body['value']->name)
                && !empty($body['value']->name)
                ? cleanSlugName($body['value']->name)
                : $this->addRandomSlugName($body['scope']);

            if (empty($body['scope'])
            && isset($body['value']->scope)
            && !empty($body['value']->scope)) {
                $body['scope'] = $body['value']->scope;
            } elseif (empty($body['scope'])
            && !isset($body['value']->scope)) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = '[0615052] scope is required';
            } elseif (!in_array($body['scope'], $listScope)) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = '[0615052] scope not valid';
            }
            $body['slug']   = $body['scope'].'-'.$metaName;

            if (isset($body['value']->status)
            && !empty($body['value']->status)) {
                $body['status'] = $body['value']->status;
            } elseif (!isset($body['status'])
            || empty($body['status'])) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = 'status is required';
            }

            if (isset($body['value']->permissions)
            && !empty($body['value']->permissions)) {
                $body['permissions'] = $body['value']->permissions;
            } elseif (!isset($body['permissions'])
            || empty($body['permissions'])) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = 'permissions is required';
            }

            if (isset($body['value']->input)
            && !empty($body['value']->input)) {
                $body['input'] = $body['value']->input;
            } elseif (!isset($body['input'])
            || empty($body['input'])) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = 'input is required';
            }

            if (isset($body['input']->email)
            && !empty($body['input']->email)) {
                $body['email'] = $body['input']->email;
            } elseif (!isset($body['email'])
            || empty($body['email'])) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = 'email is required';
            }

            if (isset($body['input']->role)
            && !empty($body['input']->role)) {
                $body['role'] = $body['input']->role;
            } elseif (!isset($body['role'])
            || empty($body['role'])) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = 'role is required';
            }

            if (isset($body['value']->answer)
            && !empty($body['value']->answer)) {
                $body['answer'] = $body['value']->answer;
            } elseif (!isset($body['answer'])
            || empty($body['answer'])) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = 'answer is required';
            }

            if (isset($body['value']->questions)
            && !empty($body['value']->questions)) {
                $body['questions'] = $body['value']->questions;
            } elseif (!isset($body['questions'])
            || empty($body['questions'])) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = 'questions is required';
            }

            if (!empty($validator) && $validator === 'unique') {
                $validate = $this->validateanswer($body, 'value');
            }

            if (is_array($validate) && count($validate) !== 0) {
                $tmpResponse['error']  = true;
                $tmpResponse['message']   = $body['slug']
                .' has been created';
            }

            if (isset($tmpResponse['error'])) {
                $response = [
                    'status'  => 'error',
                    'code'    => 503,
                    'message' => $tmpResponse['message'],
                    'data'    => null
                ];
            } else {
                $tmpResponse['success'] = true;
                $body['value']  = jsonEncode($inputValue);
                $insertData     = $this->addAnswer($body);
                $response       = $insertData->where([
                    'answerSlug' => $body['slug']
                ])->first();
                $response       = aliasResponseValue(
                    $response,
                    'answer',
                    'one',
                    $lang
                );
            }
        }

        if (isset($tmpResponse['error'])
        && isset($tmpResponse['success'])) {
            $tmpResponse['status'] = 'success';
            $tmpResponse['code'] = 207;
            $tmpResponse['message'] = 'multi status';
        } elseif (isset($tmpResponse['error'])) {
            $tmpResponse['status'] = 'error';
            $tmpResponse['code'] = 503;
            $tmpResponse['message'] = 'failed to add data';
        }

        return [
            'status'  => $tmpResponse['status'],
            'code'    => $tmpResponse['code'],
            'message' => $tmpResponse['message'],
            'data'    => $response
        ];
    }

    public function scopeUpdateanswer($query, $input = [], $validator = '')
    {
        $inputValue  = isset($input['value']) ? json_decode($input['value']) : null;
        $scope       = isset($input['scope']) ? cleanSlugName($input['scope']) : null;
        $listScope   = isset($input['listScope']) ? $input['listScope'] : ['content'];
        $lang        = isset($input['lang']) && !empty($input['lang'])
        ? $input['lang'] : 'all';
        $body        = $input;
        $response    = [];
        $tmpResponse = [
            'status' => 'success',
            'code'   => 200,
            'message'=> 'updated data'
        ];

        if (is_array($inputValue)) {
            $inputValue = flattenArray($inputValue);
            foreach ($inputValue as $key => $value) {
                $body['scope']  = $scope;
                $body['value']  = $inputValue[$key];
                $metaName       = isset($body['value']->name)
                ? cleanSlugName($body['value']->name) : null;

                if (empty($body['scope'])
                && isset($body['value']->scope)
                && !empty($body['value']->scope)) {
                    $body['scope'] = $body['value']->scope;
                } elseif (empty($body['scope'])
                && !isset($body['value']->scope)) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = '[0615052] scope is required';
                } elseif (!in_array($body['scope'], $listScope)) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = '[0615052] scope not valid';
                }
                $body['slug']   = $body['scope'].'-'.$metaName;

                if (isset($body['value']->status)
                && !empty($body['value']->status)) {
                    $body['status'] = $body['value']->status;
                }

                if (isset($body['value']->permissions)
                && !empty($body['value']->permissions)) {
                    $body['permissions'] = $body['value']->permissions;
                }

                $getData     = $this->getanswer([
                    'slug'     => $body['slug']
                ]);

                if (count($getData->get()) === 0) {
                    //return error message
                    $tmpResponse['error'][$key]  = true;
                    $tmpResponse['message']     = '[1503051] data not found';
                }

                if (isset($tmpResponse['error'][$key])) {
                    $response[$key] = [
                        'status'  => 'error',
                        'code'    => 503,
                        'message' => $tmpResponse['message'],
                        'data'    => null
                    ];
                } else {
                    $tmpResponse['success'][$key] = true;
                    $getResponse  = $getData->first();
                    $body['value']   = jsonEncode($inputValue[$key]);
                    $getData->update([
                        'answerSlug'           => isset($body['slug']) ?
                                $body['slug']      : $getResponse['answerSlug'],
                        'answerUserEmail'       => isset($body['email']) ?
                                $body['email']  : $getResponse['answerUserEmail'],
                        'answerUserRole'       => isset($body['role']) ?
                                $body['role']  : $getResponse['answerUserRole'],
                        'answerIsParent'       => isset($body['isParent']) ?
                                $body['isParent']  : $getResponse['answerIsParent'],
                        'answerParentId'       => isset($body['parentId']) ?
                                $body['parentId']  : $getResponse['answerParentId'],
                        'answerStorageUrl'     => isset($body['storageUrl']) ?
                                $body['storageUrl']: $getResponse['answerStorageUrl'],
                        'answerScope'          => !empty($body['scope']) ?
                                $body['scope']     : $getResponse['answerScope'],
                        'answerRole'        => isset($body['role']) ?
                                $body['role']   : $getResponse['answerRole'],
                        'answerValue'          => isset($body['value']) ?
                                $body['value']     : $getResponse['answerValue'],
                        'answerStatus'         => isset($body['status']) ?
                                $body['status']    : $getResponse['answerStatus'],
                        'answerPermissions'    => isset($body['permissions']) ?
                                $body['permissions']: $getResponse['answerPermissions']
                    ]);
                    $response[$key]       = aliasResponseValue(
                        $getData->first(),
                        'answer',
                        'one',
                        $lang
                    );
                }

                if (isset($body['parentId'])) {
                    unset($body['parentId']);
                }
            }
        } else {
            if (!empty($inputValue)) {
                $body['scope']  = $scope;
                $body['value']  = $inputValue;
                $metaName       = isset($body['value']->name)
                ? cleanSlugName($body['value']->name) : null;

                if (empty($body['scope'])
                && isset($body['value']->scope)
                && !empty($body['value']->scope)) {
                    $body['scope'] = $body['value']->scope;
                } elseif (empty($body['scope'])
                && !isset($body['value']->scope)) {
                    $tmpResponse['error'] = true;
                    $tmpResponse['message']     = '[0615052] scope is required';
                } elseif (!in_array($body['scope'], $listScope)) {
                    $tmpResponse['error'] = true;
                    $tmpResponse['message']     = '[0615052] scope not valid';
                }

                $body['slug']   = $body['scope'].'-'.$metaName;

                if (isset($body['value']->status)
                && !empty($body['value']->status)) {
                    $body['status'] = $body['value']->status;
                }

                if (isset($body['value']->permissions)
                && !empty($body['value']->permissions)) {
                    $body['permissions'] = $body['value']->permissions;
                }

            } elseif (!isset($body['slug'])) {
                $body['slug']   = null;
            }

            $getData     = $this->getanswer([
                'slug'   => $body['slug']
            ]);

            if (count($getData->get()) === 0) {
                //return error message
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = $body['slug'].',[1503052] data not found';
            }

            if (isset($tmpResponse['error'])) {
                $response = [
                    'status'  => 'error',
                    'code'    => 503,
                    'message' => $tmpResponse['message'],
                    'data'    => null
                ];
            } else {
                $tmpResponse['success'] = true;
                $getResponse  = $getData->first();
                if (isset($tmpResponse['error'])
                && $tmpResponse['error']) {
                    $response = [
                        'status'  => 'error',
                        'code'    => 403,
                        'message' => $body['slug'].' forbidden to write',
                        'data'    => null
                    ];
                } else {
                    $body['value']   = jsonEncode($inputValue);
                    $getData->update([
                        'answerSlug'           => isset($body['slug']) ?
                                $body['slug']      : $getResponse['answerSlug'],
                        'answerUserEmail'       => isset($body['email']) ?
                                $body['email']  : $getResponse['answerUserEmail'],
                        'answerScore'       => isset($body['score']) ?
                                $body['score']  : $getResponse['answerScore'],
                        'answerUserRole'       => isset($body['role']) ?
                                $body['role']  : $getResponse['answerUserRole'],
                        'answerIsParent'       => isset($body['isParent']) ?
                                $body['isParent']  : $getResponse['answerIsParent'],
                        'answerParentId'       => isset($body['parentId']) ?
                                $body['parentId']  : $getResponse['answerParentId'],
                        'answerStorageUrl'     => isset($body['storageUrl']) ?
                                $body['storageUrl']: $getResponse['answerStorageUrl'],
                        'answerScope'          => !empty($body['scope']) ?
                                $body['scope']     : $getResponse['answerScope'],
                        'answerRole'        => isset($body['role']) ?
                                $body['role']   : $getResponse['answerRole'],
                        'answerValue'          => isset($body['value']) ?
                                $body['value']     : $getResponse['answerValue'],
                        'answerStatus'         => isset($body['status']) ?
                                $body['status']    : $getResponse['answerStatus'],
                        'answerPermissions'    => isset($body['permissions']) ?
                                $body['permissions']: $getResponse['answerPermissions']
                    ]);
                    $response = aliasResponseValue(
                        $getData->first(),
                        'answer',
                        'one',
                        $lang
                    );
                }
            }
        }

        if (isset($tmpResponse['error'])
        && isset($tmpResponse['success'])) {
            $tmpResponse['status'] = 'success';
            $tmpResponse['code'] = 207;
            $tmpResponse['message'] = 'multi status';
        } elseif (isset($tmpResponse['error'])) {
            $tmpResponse['status'] = 'error';
            $tmpResponse['code'] = 503;
            $tmpResponse['message'] = 'failed to edit data';
        }

        return [
            'status'  => $tmpResponse['status'],
            'code'    => $tmpResponse['code'],
            'message' => $tmpResponse['message'],
            'data'    => $response
        ];
    }

    public function scopeValidateanswer($query, $input = [], $field = 'value')
    {
        $inputValue = is_array($input['value']) ?
        json_decode($input['value']) : $input['value'];
        $result     = [];

        if (is_array($inputValue) && $field === 'value') {
            $body = $input;
            $data = [];
            $inputValue = flattenArray($inputValue);
            foreach ($inputValue as $key => $value) {
                if (!isset($inputValue[$key]->name)
                && empty($inputValue[$key]->name)) {
                    $data[$key]= $inputValue[$key]->name;
                }

                $getData = $this->getanswer([
                    'slug' => $inputValue[$key]->name
                ]);

                if (count($getData->get()) !== 0) {
                    $data[$key] = $inputValue[$key]->name;
                }
            }
            if (isset($data[0])) {
                $result = [
                    'status'  => 'error',
                    'code'    => 503,
                    'message' => 'data has been created',
                    'data'    => [
                        'name' => $data
                    ]
                ];
            }
        } elseif ($field === 'value') {
            if (!isset($input['slug']) && empty($input['slug'])) {
                $result = [
                    'status'  => 'error',
                    'code'    => 404,
                    'message' => 'slug not found',
                    'data'    => null
                ];
            }

            $getData = $this->getanswer(['slug' => $input['slug']]);
            if (count($getData->get()) !== 0) {
                //return error message
                $result = [
                    'status'  => 'error',
                    'code'    => 503,
                    'message' => 'data has been created',
                    'data'    => null
                ];
            }
        }
        return $result;
    }

    public function scopeGetanswer($query, $param = [], $union = null)
    {
        $scope      = '';
        $condition  = [];

        if (isset($param['scope']) && !empty($param['scope'])) {
            $scope = $param['scope'];
        } elseif (isset($param['uri']) && !empty($param['uri'])) {
            $scope = $param['uri'];
        }

        if (!empty($scope)) {
            array_push($condition, [
                'param' => 'answerScope',
                'value' => $scope
            ]);
        }

        if (isset($param['slug']) && !empty($param['slug'])) {
            array_push($condition, [
                'param' => 'answerSlug',
                'value' => cleanSlugName($param['slug'])
            ]);
        }

        if (isset($param['search']) && !empty($param['search'])) {
            array_push($condition, [
                'param' => 'answerValue',
                'value' => $param['search']
            ]);
        }

        if (isset($param['status']) && !empty($param['status'])) {
            array_push($condition, [
                'param' => 'answerStatus',
                'value' => $param['status']
            ]);
        }

        if (isset($param['permissions']) && !empty($param['permissions'])) {
            array_push($condition, [
                'param' => 'answerPermissions',
                'value' => $param['permissions']
            ]);
        }

        $getData = $this->getContentJson(
            $condition,
            'answerValue',
            $union
        );

        return $getData;
    }
}
