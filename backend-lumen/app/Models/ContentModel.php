<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class contentModel extends Model
{

    //
    protected static $elq = __CLASS__;
    protected $table      = 'content';
    protected $primaryKey = 'contentId';
    protected $dates      = ['contentDeletedDate'];

    protected $casts = [
        'contentId'    => 'int',
        'contentValue' => 'array'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'contentValue',
        'contentStorageUrl',
        'contentScope',
        'contentSlug',
        'contentStatus',
        'contentPermissions',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $timestamps = false;
    const CREATED_AT = 'contentCreatedDate';
    const UPDATED_AT = 'contentUpdatedDate';

    protected $fieldRules = [
        'contentSlug'       => ['',''],
        'contentValue'      => ['',''],
        'contentStorageUrl' => ['',''],
        'contentUserId'     => ['','']
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function scopeAddcontent($query, $body = [])
    {
        $query->insert([
            'contentSlug'           =>  $body['slug'],
            'contentIsParent'       =>  isset($body['isParent'])
            ? $body['isParent'] : null,
            'contentParentId'       =>  isset($body['parentId'])
            ? $body['parentId'] : null,
            'contentStorageUrl'     =>  isset($body['storageUrl'])
            ? $body['storageUrl'] : null,
            'contentScope'          =>  $body['scope'],
            'contentValue'          =>  $body['value'],
            'contentStatus'         => isset($body['status'])
            ? $body['status'] : null,
            'contentPermissions'    => isset($body['permissions'])
            ? $body['permissions'] : '755'
        ]);
    }

    public function scopeGetContentJson($query, $body = [], $condition = '', $unionData = null)
    {
        $query->selectRaw('contentId,contentSlug,contentValue,contentScope,
        contentPermissions,contentStatus,contentUpdatedDate,
        contentCreatedDate,contentIsParent,contentParentId');
        $explode = explode(',', $condition);
        $uri     = '->attr-uri';
        $result  = convertArrayParam($body, 'contentScope');

        if (isset($explode[1])) {
            foreach ($explode as $key => $value) {
                # code...
                $cond[$key] = $value;
            }
        } else {
            $cond = $condition;
        }

        if ($cond && !empty($cond)) {
            $query->whereNotNull($cond);
        }

        if ($result) {
            $uri = '->name';
        }

        if (isset($body[0])
        && isset($body[0]['param'])
        && isset($body[0]['value'])) {
            foreach ($body as $key => $value) {
                # code...
                $obj[$key] = $body[$key];
                if (!isset($body[$key]['param'])
                || !isset($body[$key]['value'])) {
                    return null;
                }
                if ($body[$key]['param'] === 'contentValue'
                && $uri === '->name') {
                    $body[$key]['value'] = str_replace('-', ' ', $body[$key]['value']);
                    $obj[$key]['param'] = $body[$key]['param'].$uri;
                } elseif ($body[$key]['param'] === 'contentValue') {
                    $obj[$key]['param'] = $body[$key]['param'].$uri;
                }
                $query->where([
                    [$obj[$key]['param'], '=', $obj[$key]['value']],
                    ['contentDeletedDate', '=', null]
                ]);
            }
        } else {
            $obj = $body;

            if (!isset($body['param'])
            || !isset($body['value'])) {
                return null;
            }
            if ($body['param'] === 'contentValue') {
                $obj['param'] = $body['param'].$uri;
            }
            $query->where($obj['param'], $obj['value']);
        }

        if (!empty($unionData)) {
            $query->union($unionData);
        }

        return $query;
    }

    public function scopeAddRandomSlugName($query, $scope = 'content', $length = 4)
    {
        $name = strtolower(generateRandomCode($length));
        $query->where('contentSlug', $name);
        if (!$query->first() || empty($query->first())) {
            return $name;
        }
        return $this->addRandomSlugName($scope, $length + 1);
    }

    public function scopeInsertcontent($query, $input = [], $validator = '')
    {
        $inputValue  = isset($input['value']) ? json_decode($input['value']) : null;
        $validate    = [];
        $scope       = isset($input['scope']) ? cleanSlugName($input['scope']) : null;
        $listScope   = isset($input['listScope']) ? $input['listScope'] : ['content'];
        $lang        = isset($input['lang']) && !empty($input['lang'])
        ? $input['lang'] : 'all';
        $body        = $input;
        $response    = [];
        $tmpResponse = [
            'status' => 'success',
            'code'   => 200,
            'message'=> 'crated data'
        ];

        if (is_array($inputValue)) {
            $inputValue = flattenArray($inputValue);
            foreach ($inputValue as $key => $value) {
                $body['scope']  = $scope;
                $body['value']  = $inputValue[$key];
                $metaName       = isset($body['value']->name)
                && !empty($body['value']->name)
                ? cleanSlugName($body['value']->name)
                : $this->addRandomSlugName($scope);

                if (empty($body['scope'])
                && isset($body['value']->scope)
                && !empty($body['value']->scope)) {
                    $body['scope'] = $body['value']->scope;
                } elseif (empty($body['scope'])
                && !isset($body['value']->scope)) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = '[] scope is required';
                } elseif (!in_array($body['scope'], $listScope)) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = '[] scope not valid';
                }

                $body['slug']   = $body['scope'].'-'.$metaName;

                if (isset($body['value']->status)
                && !empty($body['value']->status)) {
                    $body['status'] = $body['value']->status;
                } elseif (!isset($body['status'])
                || empty($body['status'])) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = 'status is required';
                }

                if (isset($body['value']->permissions)
                && !empty($body['value']->permissions)) {
                    $body['permissions'] = $body['value']->permissions;
                } elseif (!isset($body['permissions'])
                || empty($body['permissions'])) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = 'permissions is required';
                }

                if (!isset($body['value']->isParent)
                && isset($body['isParent'])) {
                    unset($body['isParent']);
                } elseif (isset($body['value']->isParent)) {
                    $body['isParent'] = $body['value']->isParent;
                }

                if (isset($body['value']->parentName)
                && !isset($body['parentId'])
                && !empty($body['value']->parentName)) {
                    $body['isParentId'] = false;
                    $parentSlug   = $body['scope'].'-'.cleanSlugName($body['value']->parentName);

                    $getData     = $this->getcontent([
                        'slug'     => $parentSlug
                    ]);

                    if (count($getData->get()) === 0) {
                        //return error message
                        $tmpResponse['error'][$key] = true;
                        $tmpResponse['message']     = 'parent name not found';
                    }

                    $getData = $getData->first();

                    if (isset($getData->contentId)
                    && !empty($getData->contentId)) {
                        $body['isParentId'] = true;
                        $body['parentId'] = $getData->contentId;
                    } else {
                        //return error message
                        $tmpResponse['error'][$key] = true;
                        $tmpResponse['message']     = 'parent id not found';
                    }
                }

                if (!empty($validator) && $validator === 'unique') {
                    $validate = $this->validatecontent($body, 'value');
                }

                if (is_array($validate) && count($validate) !== 0) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = $body['slug']
                    .' has been created';
                }

                if (isset($tmpResponse['error'][$key])) {
                    $response[$key] = [
                        'status'  => 'error',
                        'code'    => 503,
                        'message' => $tmpResponse['message'],
                        'data'    => null
                    ];
                } else {
                    $tmpResponse['success'][$key]  = true;
                    $body['value']  = jsonEncode($inputValue[$key]);
                    $insertData     = entity(null, 'contentModel')
                    ::addcontent($body);
                    $response[$key] = $insertData->where([
                        'contentSlug' => $body['slug']
                    ])->first();
                    $response[$key] = aliasResponseValue(
                        $response[$key],
                        'content',
                        'one',
                        $lang
                    );
                }
                if (isset($body['parentId'])
                && isset($body['isParentId'])
                && $body['isParentId']) {
                    unset($body['parentId']);
                }
            }
        } else {
            $body['scope']  = $scope;
            $body['value']  = $inputValue;
            $metaName       = isset($body['value']->name)
                && !empty($body['value']->name)
                ? cleanSlugName($body['value']->name)
                : $this->addRandomSlugName($body['scope']);

            if (empty($body['scope'])
            && isset($body['value']->scope)
            && !empty($body['value']->scope)) {
                $body['scope'] = $body['value']->scope;
            } elseif (empty($body['scope'])
            && !isset($body['value']->scope)) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = '[0615052] scope is required';
            } elseif (!in_array($body['scope'], $listScope)) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = '[0615052] scope not valid';
            }
            $body['slug']   = $body['scope'].'-'.$metaName;

            if (isset($body['value']->status)
            && !empty($body['value']->status)) {
                $body['status'] = $body['value']->status;
            } elseif (!isset($body['status'])
            || empty($body['status'])) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = 'status is required';
            }

            if (isset($body['value']->permissions)
            && !empty($body['value']->permissions)) {
                $body['permissions'] = $body['value']->permissions;
            } elseif (!isset($body['permissions'])
            || empty($body['permissions'])) {
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = 'permissions is required';
            }

            if (!empty($validator) && $validator === 'unique') {
                $validate = $this->validatecontent($body, 'value');
            }

            if (is_array($validate) && count($validate) !== 0) {
                $tmpResponse['error']  = true;
                $tmpResponse['message']   = $body['slug']
                .' has been created';
            }

            if (isset($tmpResponse['error'])) {
                $response = [
                    'status'  => 'error',
                    'code'    => 503,
                    'message' => $tmpResponse['message'],
                    'data'    => null
                ];
            } else {
                $tmpResponse['success'] = true;
                $body['value']  = jsonEncode($inputValue);
                $insertData     = $this->addcontent($body);
                $response       = $insertData->where([
                    'contentSlug' => $body['slug']
                ])->first();
                $response       = aliasResponseValue(
                    $response,
                    'content',
                    'one',
                    $lang
                );
            }
        }

        if (isset($tmpResponse['error'])
        && isset($tmpResponse['success'])) {
            $tmpResponse['status'] = 'success';
            $tmpResponse['code'] = 207;
            $tmpResponse['message'] = 'multi status';
        } elseif (isset($tmpResponse['error'])) {
            $tmpResponse['status'] = 'error';
            $tmpResponse['code'] = 503;
            $tmpResponse['message'] = 'failed to add data';
        }

        return [
            'status'  => $tmpResponse['status'],
            'code'    => $tmpResponse['code'],
            'message' => $tmpResponse['message'],
            'data'    => $response
        ];
    }

    public function scopeUpdatecontent($query, $input = [], $validator = '')
    {
        $inputValue  = isset($input['value']) ? json_decode($input['value']) : null;
        $scope       = isset($input['scope']) ? cleanSlugName($input['scope']) : null;
        $listScope   = isset($input['listScope']) ? $input['listScope'] : ['content'];
        $lang        = isset($input['lang']) && !empty($input['lang'])
        ? $input['lang'] : 'all';
        $body        = $input;
        $response    = [];
        $tmpResponse = [
            'status' => 'success',
            'code'   => 200,
            'message'=> 'updated data'
        ];

        if (is_array($inputValue)) {
            $inputValue = flattenArray($inputValue);
            foreach ($inputValue as $key => $value) {
                $body['scope']  = $scope;
                $body['value']  = $inputValue[$key];
                $metaName       = isset($body['value']->name)
                ? cleanSlugName($body['value']->name) : null;

                if (empty($body['scope'])
                && isset($body['value']->scope)
                && !empty($body['value']->scope)) {
                    $body['scope'] = $body['value']->scope;
                } elseif (empty($body['scope'])
                && !isset($body['value']->scope)) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = '[0615052] scope is required';
                } elseif (!in_array($body['scope'], $listScope)) {
                    $tmpResponse['error'][$key] = true;
                    $tmpResponse['message']     = '[0615052] scope not valid';
                }
                $body['slug']   = $body['scope'].'-'.$metaName;

                if (isset($body['value']->status)
                && !empty($body['value']->status)) {
                    $body['status'] = $body['value']->status;
                }

                if (isset($body['value']->permissions)
                && !empty($body['value']->permissions)) {
                    $body['permissions'] = $body['value']->permissions;
                }

                $getData     = $this->getcontent([
                    'slug'     => $body['slug']
                ]);

                if (count($getData->get()) === 0) {
                    //return error message
                    $tmpResponse['error'][$key]  = true;
                    $tmpResponse['message']     = '[1503051] data not found';
                }

                if (isset($tmpResponse['error'][$key])) {
                    $response[$key] = [
                        'status'  => 'error',
                        'code'    => 503,
                        'message' => $tmpResponse['message'],
                        'data'    => null
                    ];
                } else {
                    $tmpResponse['success'][$key] = true;
                    $getResponse  = $getData->first();
                    $body['value']   = jsonEncode($inputValue[$key]);
                    $getData->update([
                        'contentSlug'           => isset($body['slug']) ?
                                $body['slug']      : $getResponse['contentSlug'],
                        'contentIsParent'       => isset($body['isParent']) ?
                                $body['isParent']  : $getResponse['contentIsParent'],
                        'contentParentId'       => isset($body['parentId']) ?
                                $body['parentId']  : $getResponse['contentParentId'],
                        'contentStorageUrl'     => isset($body['storageUrl']) ?
                                $body['storageUrl']: $getResponse['contentStorageUrl'],
                        'contentScope'          => !empty($body['scope']) ?
                                $body['scope']     : $getResponse['contentScope'],
                        'contentRole'        => isset($body['role']) ?
                                $body['role']   : $getResponse['contentRole'],
                        'contentValue'          => isset($body['value']) ?
                                $body['value']     : $getResponse['contentValue'],
                        'contentStatus'         => isset($body['status']) ?
                                $body['status']    : $getResponse['contentStatus'],
                        'contentPermissions'    => isset($body['permissions']) ?
                                $body['permissions']: $getResponse['contentPermissions']
                    ]);
                    $response[$key]       = aliasResponseValue(
                        $getData->first(),
                        'content',
                        'one',
                        $lang
                    );
                }

                if (isset($body['parentId'])) {
                    unset($body['parentId']);
                }
            }
        } else {
            if (!empty($inputValue)) {
                $body['scope']  = $scope;
                $body['value']  = $inputValue;
                $metaName       = isset($body['value']->name)
                ? cleanSlugName($body['value']->name) : null;

                if (empty($body['scope'])
                && isset($body['value']->scope)
                && !empty($body['value']->scope)) {
                    $body['scope'] = $body['value']->scope;
                } elseif (empty($body['scope'])
                && !isset($body['value']->scope)) {
                    $tmpResponse['error'] = true;
                    $tmpResponse['message']     = '[0615052] scope is required';
                } elseif (!in_array($body['scope'], $listScope)) {
                    $tmpResponse['error'] = true;
                    $tmpResponse['message']     = '[0615052] scope not valid';
                }

                $body['slug']   = $body['scope'].'-'.$metaName;

                if (isset($body['value']->status)
                && !empty($body['value']->status)) {
                    $body['status'] = $body['value']->status;
                }

                if (isset($body['value']->permissions)
                && !empty($body['value']->permissions)) {
                    $body['permissions'] = $body['value']->permissions;
                }

            } elseif (!isset($body['slug'])) {
                $body['slug']   = null;
            }

            $getData     = $this->getcontent([
                'slug'   => $body['slug']
            ]);

            if (count($getData->get()) === 0) {
                //return error message
                $tmpResponse['error'] = true;
                $tmpResponse['message']     = $body['slug'].',[1503052] data not found';
            }

            if (isset($tmpResponse['error'])) {
                $response = [
                    'status'  => 'error',
                    'code'    => 503,
                    'message' => $tmpResponse['message'],
                    'data'    => null
                ];
            } else {
                $tmpResponse['success'] = true;
                $getResponse  = $getData->first();
                if (isset($tmpResponse['error'])
                && $tmpResponse['error']) {
                    $response = [
                        'status'  => 'error',
                        'code'    => 403,
                        'message' => $body['slug'].' forbidden to write',
                        'data'    => null
                    ];
                } else {
                    $body['value']   = jsonEncode($inputValue);
                    $getData->update([
                        'contentSlug'           => isset($body['slug']) ?
                                $body['slug']      : $getResponse['contentSlug'],
                        'contentScore'       => isset($body['score']) ?
                                $body['score']  : $getResponse['contentScore'],
                        'contentIsParent'       => isset($body['isParent']) ?
                                $body['isParent']  : $getResponse['contentIsParent'],
                        'contentParentId'       => isset($body['parentId']) ?
                                $body['parentId']  : $getResponse['contentParentId'],
                        'contentStorageUrl'     => isset($body['storageUrl']) ?
                                $body['storageUrl']: $getResponse['contentStorageUrl'],
                        'contentScope'          => !empty($body['scope']) ?
                                $body['scope']     : $getResponse['contentScope'],
                        'contentRole'        => isset($body['role']) ?
                                $body['role']   : $getResponse['contentRole'],
                        'contentValue'          => isset($body['value']) ?
                                $body['value']     : $getResponse['contentValue'],
                        'contentStatus'         => isset($body['status']) ?
                                $body['status']    : $getResponse['contentStatus'],
                        'contentPermissions'    => isset($body['permissions']) ?
                                $body['permissions']: $getResponse['contentPermissions']
                    ]);
                    $response = aliasResponseValue(
                        $getData->first(),
                        'content',
                        'one',
                        $lang
                    );
                }
            }
        }

        if (isset($tmpResponse['error'])
        && isset($tmpResponse['success'])) {
            $tmpResponse['status'] = 'success';
            $tmpResponse['code'] = 207;
            $tmpResponse['message'] = 'multi status';
        } elseif (isset($tmpResponse['error'])) {
            $tmpResponse['status'] = 'error';
            $tmpResponse['code'] = 503;
            $tmpResponse['message'] = 'failed to edit data';
        }

        return [
            'status'  => $tmpResponse['status'],
            'code'    => $tmpResponse['code'],
            'message' => $tmpResponse['message'],
            'data'    => $response
        ];
    }

    public function scopeValidatecontent($query, $input = [], $field = 'value')
    {
        $inputValue = is_array($input['value']) ?
        json_decode($input['value']) : $input['value'];
        $result     = [];

        if (is_array($inputValue) && $field === 'value') {
            $body = $input;
            $data = [];
            $inputValue = flattenArray($inputValue);
            foreach ($inputValue as $key => $value) {
                if (!isset($inputValue[$key]->name)
                && empty($inputValue[$key]->name)) {
                    $data[$key]= $inputValue[$key]->name;
                }

                $getData = $this->getcontent([
                    'slug' => $inputValue[$key]->name
                ]);

                if (count($getData->get()) !== 0) {
                    $data[$key] = $inputValue[$key]->name;
                }
            }
            if (isset($data[0])) {
                $result = [
                    'status'  => 'error',
                    'code'    => 503,
                    'message' => 'data has been created',
                    'data'    => [
                        'name' => $data
                    ]
                ];
            }
        } elseif ($field === 'value') {
            if (!isset($input['slug']) && empty($input['slug'])) {
                $result = [
                    'status'  => 'error',
                    'code'    => 404,
                    'message' => 'slug not found',
                    'data'    => null
                ];
            }

            $getData = $this->getcontent(['slug' => $input['slug']]);
            if (count($getData->get()) !== 0) {
                //return error message
                $result = [
                    'status'  => 'error',
                    'code'    => 503,
                    'message' => 'data has been created',
                    'data'    => null
                ];
            }
        }
        return $result;
    }

    public function scopeGetcontent($query, $param = [], $union = null)
    {
        $scope      = '';
        $condition  = [];

        if (isset($param['scope']) && !empty($param['scope'])) {
            $scope = $param['scope'];
        } elseif (isset($param['uri']) && !empty($param['uri'])) {
            $scope = $param['uri'];
        }

        if (!empty($scope)) {
            array_push($condition, [
                'param' => 'contentScope',
                'value' => $scope
            ]);
        }

        if (isset($param['slug']) && !empty($param['slug'])) {
            array_push($condition, [
                'param' => 'contentSlug',
                'value' => cleanSlugName($param['slug'])
            ]);
        }

        if (isset($param['search']) && !empty($param['search'])) {
            array_push($condition, [
                'param' => 'contentValue',
                'value' => $param['search']
            ]);
        }

        if (isset($param['status']) && !empty($param['status'])) {
            array_push($condition, [
                'param' => 'contentStatus',
                'value' => $param['status']
            ]);
        }

        if (isset($param['permissions']) && !empty($param['permissions'])) {
            array_push($condition, [
                'param' => 'contentPermissions',
                'value' => $param['permissions']
            ]);
        }

        $getData = $this->getContentJson(
            $condition,
            'contentValue',
            $union
        );

        return $getData;
    }
}
